import Vue from 'vue'
import Router from 'vue-router'
import AdminHome from '@/components/AdminHome'
import NewContact from '../components/NewContact'
import Contacts from '../components/Contacts'
import Products from '../components/Products'
import NewProduct from '../components/NewProduct'
import Categories from '../components/Categories'
import NewCatergory from '../components/NewCategory'
import Cms from '../components/Cms'
import NewCms from '../components/NewCms'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'AdminHome',
      component: AdminHome
    },
    {
      path: '/newcontact',
      name: 'new-contact',
      component: NewContact
    },
    {
      path: '/contacts',
      name: 'contacts',
      component: Contacts
    },
    {
      path: '/products',
      name: 'products',
      component: Products
    },
    {
      path: '/newproduct',
      name: 'new-product',
      component: NewProduct
    },
    {
      path: '/categories',
      name: 'categories',
      component: Categories
    },
    {
      path: '/newcategory',
      name: 'new-category',
      component: NewCatergory
    },
    {
      path: '/cms',
      name: 'cms',
      component: Cms
    },
    {
      path: '/newcms',
      name: 'newcms',
      component: NewCms
    },
    
  ]
})
